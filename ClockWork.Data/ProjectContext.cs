﻿using ClockWork.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClockWork.Data
{
    public class ProjectContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<EmployeeProject> EmployeeProject { get; set; }

        public DbSet<Project> Projects { get; set; }
        public DbSet<ProjectTask> Tasks { get; set; }

        public ProjectContext(DbContextOptions<ProjectContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProjectTask>()
                .HasOne(p => p.Project)
                .WithMany(t => t.Tasks)
                .HasForeignKey(p => p.ProjectId);

            modelBuilder.Entity<Project>()
            .HasMany(p => p.EmployeeProjects);


            //.HasOne(p => p.EmployeeProjects)
            //.WithMany(t => t.Projects)
            //.HasForeignKey(p => p.EmployeeProjectId);

            modelBuilder.Entity<Employee>()
            .HasMany(p => p.EmployeeProjects);
        }
    }
}
