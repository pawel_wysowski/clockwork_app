﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ClockWork.Migrations
{
    public partial class CorrectedRelations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Employees_EmployeeProject_EmployeeProjectId",
                table: "Employees");

            migrationBuilder.DropForeignKey(
                name: "FK_Projects_EmployeeProject_EmployeeProjectId",
                table: "Projects");

            migrationBuilder.DropIndex(
                name: "IX_Projects_EmployeeProjectId",
                table: "Projects");

            migrationBuilder.DropIndex(
                name: "IX_Employees_EmployeeProjectId",
                table: "Employees");

            migrationBuilder.DropColumn(
                name: "EmployeeProjectId",
                table: "Projects");

            migrationBuilder.DropColumn(
                name: "EmployeeProjectId",
                table: "Employees");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeProject_EmployeeId",
                table: "EmployeeProject",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeProject_ProjectId",
                table: "EmployeeProject",
                column: "ProjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_EmployeeProject_Employees_EmployeeId",
                table: "EmployeeProject",
                column: "EmployeeId",
                principalTable: "Employees",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EmployeeProject_Projects_ProjectId",
                table: "EmployeeProject",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EmployeeProject_Employees_EmployeeId",
                table: "EmployeeProject");

            migrationBuilder.DropForeignKey(
                name: "FK_EmployeeProject_Projects_ProjectId",
                table: "EmployeeProject");

            migrationBuilder.DropIndex(
                name: "IX_EmployeeProject_EmployeeId",
                table: "EmployeeProject");

            migrationBuilder.DropIndex(
                name: "IX_EmployeeProject_ProjectId",
                table: "EmployeeProject");

            migrationBuilder.AddColumn<int>(
                name: "EmployeeProjectId",
                table: "Projects",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "EmployeeProjectId",
                table: "Employees",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Projects_EmployeeProjectId",
                table: "Projects",
                column: "EmployeeProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Employees_EmployeeProjectId",
                table: "Employees",
                column: "EmployeeProjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_Employees_EmployeeProject_EmployeeProjectId",
                table: "Employees",
                column: "EmployeeProjectId",
                principalTable: "EmployeeProject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_EmployeeProject_EmployeeProjectId",
                table: "Projects",
                column: "EmployeeProjectId",
                principalTable: "EmployeeProject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
