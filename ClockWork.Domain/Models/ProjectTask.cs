﻿using System;

namespace ClockWork.Domain.Models
{
    public class ProjectTask
    {
        public int Id { get; set; }
        public string TaskValue { get; set; }
        public TimeSpan TimeSpan { get; set; }

        public int ProjectId { get; set; }
        public Project Project { get; set; }
    }
}