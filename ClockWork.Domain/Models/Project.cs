﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClockWork.Domain.Models
{
    public class Project
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<ProjectTask> Tasks { get; set; }

        public List<EmployeeProject> EmployeeProjects { get; set; }
    }
}
