﻿using System.Collections.Generic;

namespace ClockWork.Domain.Models
{
    public class EmployeeProject
    {
        public int Id { get; set; }

        public int EmployeeId { get; set; }
        public Employee Employee { get; set; }

        public int ProjectId { get; set; }
        public Project Project { get; set; }
    }
}