﻿using System.Collections.Generic;

namespace ClockWork.Services.DTOs
{
    public class ProjectDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<ProjectTaskDTO> Tasks { get; set; }

        public EmployeeProjectDTO EmployeeProject { get; set; }
        public int EmployeeProjectId { get; set; }
    }
}