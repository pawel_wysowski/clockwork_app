﻿using System.Collections.Generic;

namespace ClockWork.Services.DTOs
{
    public class EmployeeProjectDTO
    {
        public int Id { get; set; }

        public int EmployeeId { get; set; }
        public List<EmployeeDTO> Employee { get; set; }

        public int ProjectId { get; set; }
        public List<ProjectDTO> Projects { get; set; }
    }
}