﻿using System;

namespace ClockWork.Services.DTOs
{
    public class ProjectTaskDTO
    {
        public int Id { get; set; }
        public string TaskValue { get; set; }
        public TimeSpan TimeSpan { get; set; }

        public int ProjectId { get; set; }
        public ProjectDTO Project { get; set; }
    }
}