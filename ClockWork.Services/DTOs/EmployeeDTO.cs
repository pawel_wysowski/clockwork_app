﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClockWork.Services.DTOs
{
    public class EmployeeDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public int EmployeeProjectId { get; set; }
        public EmployeeProjectDTO EmployeeProject { get; set; }
    }
}
