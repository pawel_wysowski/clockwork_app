﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClockWork.Data;
using ClockWork.Domain.Models;
using ClockWork.Services.DTOs;
using Microsoft.EntityFrameworkCore;
namespace ClockWork.Services.Services
{
    public class ProjectService : IProjectService
    {
        private ProjectContext projectContext;

        public ProjectService(ProjectContext projectContext)
        {
            this.projectContext = projectContext;
        }

        public void AddNewProject(ProjectDTO project)
        {
            projectContext.Projects.Add(AutoMapper.Mapper.Map<ProjectDTO,Project>(project));
        }

        public List<ProjectDTO> GetProjectsList()
        {
            var allProjects = projectContext.Projects.Include(project => project.Tasks).ToList();
            var allProjectsDTOs = AutoMapper.Mapper.Map<List<Project>, List<ProjectDTO>>(allProjects);
            return allProjectsDTOs;
        }
    }
}
