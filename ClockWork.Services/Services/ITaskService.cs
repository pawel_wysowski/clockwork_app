﻿using ClockWork.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClockWork.Services.Services
{
    public interface ITaskService
    {
        void AddNewTaskToProject(ProjectTaskDTO projectTaskDTO);
        List<ProjectTaskDTO> GetAllTasksFromProject(int projectId);
    }
}
