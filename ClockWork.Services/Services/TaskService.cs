﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClockWork.Data;
using ClockWork.Domain.Models;
using ClockWork.Services.DTOs;
using Microsoft.EntityFrameworkCore;

namespace ClockWork.Services.Services
{
    public class TaskService : ITaskService
    {
        private ProjectContext projectContext;

        public TaskService(ProjectContext projectContext)
        {
            this.projectContext = projectContext;
        }

        public void AddNewTaskToProject(ProjectTaskDTO projectTaskDTO)
        {
            var projectTask = AutoMapper.Mapper.Map<ProjectTaskDTO, ProjectTask>(projectTaskDTO);

            projectContext.Tasks.Add(projectTask);
        }

        public List<ProjectTaskDTO> GetAllTasksFromProject(int projectId)
        {
            List<ProjectTask> taskListFromProject = projectContext.Tasks.Where(task => task.Project.Id.Equals(projectId)).Include(task => task.Project).ToList();
            var projectTasksDTOs = AutoMapper.Mapper.Map<List<ProjectTask>, List<ProjectTaskDTO>> (taskListFromProject);

            return projectTasksDTOs;
        }
    }
}
