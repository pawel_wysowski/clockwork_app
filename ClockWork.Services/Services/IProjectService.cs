﻿using ClockWork.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClockWork.Services.Services
{
    public interface IProjectService
    {
        void AddNewProject(ProjectDTO project);
        List<ProjectDTO> GetProjectsList();
    }
}
