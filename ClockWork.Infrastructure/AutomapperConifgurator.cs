﻿using ClockWork.Domain.Models;
using ClockWork.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClockWork.Infrastructure
{
    public class AutomapperConifgurator
    {
        public static void Configure()
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Employee, EmployeeDTO>();
                cfg.CreateMap<Project, ProjectDTO>();
            });
        }
    }
}
