﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClockWork.Services.DTOs;
using ClockWork.Services.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ClockWork.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private ITaskService _taskService;

        public TasksController(ITaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet("{projectId}")]
        public IActionResult GetAllTasksFromProject(int projectID)
        {
            var tasks = _taskService.GetAllTasksFromProject(projectID);

            if (tasks.Any())
            {
                return Ok(tasks);
            }
            else
            {
                return NotFound(projectID);
            }
        }

        [HttpPost]
        public IActionResult AddNewTaskToProject(ProjectTaskDTO projectTaskDTO)
        {
            _taskService.AddNewTaskToProject(projectTaskDTO);
            return Ok();
        }
    }
}