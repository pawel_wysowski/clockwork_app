﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClockWork.Services.DTOs;
using ClockWork.Services.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ClockWork.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private IProjectService _projectService;

        public ProjectsController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public IActionResult GetAllProjects()
        {
            var allProjects = _projectService.GetProjectsList();

            if (allProjects.Any())
            {
                return Ok(allProjects);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        public IActionResult AddNewProject(ProjectDTO projectDTO)
        {
            _projectService.AddNewProject(projectDTO);
            return Ok();
        }
    }
}